#define FREEGLUT_STATIC
#define GLUT_DISABLE_ATEXIT_HACK
#include <GL/freeglut.h>
#include <math.h>
#include <stdlib.h>
#include "vector"
using namespace std;

int intWinWidth = 600; //Default window size
int intWinHeight = 400;

float fltFOV = 70; //Field Of View
float fltZoom = 1.0; //Zoom amount 
GLfloat  X0 = 0.0, Y0 = 50.0, Z0 = 200.0; // camera/viewing co-ordinate origin
GLfloat xref = 0.0, yref = 50.0, zref = 0.0;// look-at point
GLfloat Vx = 0.0, Vy = 1.0, Vz = 0.0; //view-up vector
float fltViewingAngle = 0; //Used for rotating camera

//image
GLint imagewidth0, imagewidth1, imagewidth2, imagewidth3, imagewidth4, imagewidth5, imagewidth6;
GLint imageheight0, imageheight1, imageheight2, imageheight3, imageheight4, imageheight5, imageheight6;
GLint pixellength0, pixellength1, pixellength2, pixellength3, pixellength4, pixellength5, pixellength6;

vector<GLubyte*>p;  // Similar to GLubyte* for program 3 but for 2 images (so a vector)
//GLuint texture[2];
GLuint texture[7];
 
GLfloat diffuseMaterial[4] = { 1.0, 1.0, 1.0, 1.0 };

// lab 9 
// Define light and material properties
void lighting() {
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 }; // Directional light
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseMaterial);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, 50.0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	// Specify which material parameters track the current color
	glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
	//glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
	glEnable(GL_COLOR_MATERIAL);
}

// from lab 10 program 4 
void ReadImage(const char path[256], GLint& imagewidth, GLint& imageheight, GLint& pixellength) {
	GLubyte* pixeldata;
	FILE* pfile;
	fopen_s(&pfile, path, "rb");
	if (pfile == 0) exit(0);
	fseek(pfile, 0x0012, SEEK_SET);
	fread(&imagewidth, sizeof(imagewidth), 1, pfile);
	fread(&imageheight, sizeof(imageheight), 1, pfile);
	pixellength = imagewidth * 3;
	while (pixellength % 4 != 0)pixellength++;
	pixellength *= imageheight;
	pixeldata = (GLubyte*)malloc(pixellength);
	if (pixeldata == 0) exit(0);
	fseek(pfile, 54, SEEK_SET);
	fread(pixeldata, pixellength, 1, pfile);
	p.push_back(pixeldata);
	fclose(pfile);
}

// from CPT205 Lab 10 ppt 
void LoadTexture() {
	// texture for paint on the table
	ReadImage("square.bmp", imagewidth0, imageheight0, pixellength0);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set pixel storage modes (in the memory)
	
	glGenTextures(7, &texture[0]); // number of texture, array of tecture names

	glBindTexture(GL_TEXTURE_2D, texture[0]); // target to which texture is bound and name of a texture 
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imagewidth0, imageheight0, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, p[0]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//texture for bookside
	ReadImage("book3.bmp", imagewidth1, imageheight1, pixellength1);
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imagewidth1, imageheight1, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, p[1]); 
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); 
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); 
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//table top
	ReadImage("wood.bmp", imagewidth2, imageheight2, pixellength2);
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imagewidth2, imageheight2, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, p[2]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//table side
	ReadImage("wood_side.bmp", imagewidth3, imageheight3, pixellength3);
	glBindTexture(GL_TEXTURE_2D, texture[3]);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imagewidth3, imageheight3, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, p[3]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//book texture
	ReadImage("bookColor3.bmp", imagewidth4, imageheight4, pixellength4);
	glBindTexture(GL_TEXTURE_2D, texture[4]);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imagewidth4, imageheight4, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, p[4]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	//book texture
	ReadImage("serious.bmp", imagewidth5, imageheight5, pixellength5);
	glBindTexture(GL_TEXTURE_2D, texture[5]);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imagewidth5, imageheight5, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, p[5]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//book texture
	ReadImage("nice.bmp", imagewidth6, imageheight6, pixellength6);
	glBindTexture(GL_TEXTURE_2D, texture[6]);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imagewidth6, imageheight6, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, p[6]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}

// from lab 10
// add pixture to pad
void Drawing() {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_FLAT);
	glEnable(GL_TEXTURE_2D);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	
	// draw paint on the table
	glPushMatrix(); // push translate
	glTranslatef(-10.0, 2.0, -10.0);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, 1.0, 0.0);
	glTexCoord2f(0.0, 0.0); glVertex3f(0.0, 1.0, 20.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(20.0, 1.0, 20.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(20.0, 1.0, 0.0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix(); // pop translate 
}


void tableLeg() {
	glPushMatrix();
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glColor3f(1.0, 1.0, 1.0); //white
	glutSolidCylinder(2.5, 70.0, 10, 10);
	glColor3f(0.0, 0.0, 0.0); //black wire
	glutWireCylinder(2.5, 70.0, 1, 1);
	glPopMatrix();
}

void desk() {
	//desk
	glPushMatrix();
	glScalef(100.0, 5.0, 60.0);
	glEnable(GL_COLOR_MATERIAL);
	glColor3f(1, 1, 1);
	glutSolidCube(1);
	glColor3f(0.0, 0.0, 0.0);
	glutWireCube(1);
	glPopMatrix();

	//deskleg
	glPushMatrix();
	glTranslatef(-40.0, 0.0, 25.0);
	tableLeg();

	glPushMatrix();
	glTranslatef(0.0, 0.0, -50.0);
	tableLeg();
	glPopMatrix();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(40.0, 0.0, 25.0);
	tableLeg();

	glPushMatrix();
	glTranslatef(0.0, 0.0, -50.0);
	tableLeg();
	glPopMatrix();

	glPopMatrix();
	//glPopMatrix();
	glDisable(GL_COLOR_MATERIAL);
	
}

void TableTexture() {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_FLAT);

	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

	// add texture for table surface
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-50.0, 2.6, 30.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(-50.0, 2.6, -30.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(50.0, 2.6, -30.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(50.0, 2.6, 30.0);
	glEnd();
	//glDisable(GL_TEXTURE_2D);

	// front side
	glBindTexture(GL_TEXTURE_2D, texture[3]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-50.0, -2.6, 30.1);
	glTexCoord2f(0.0, 1.0); glVertex3f(-50.0, 2.6, 30.1);
	glTexCoord2f(1.0, 1.0); glVertex3f(50.0, 2.6, 30.1);
	glTexCoord2f(1.0, 0.0); glVertex3f(50.0, -2.6, 30.1);
	glEnd();

	//back side
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-50.0, -2.6, -30.1);
	glTexCoord2f(0.0, 1.0); glVertex3f(-50.0, 2.6, -30.1);
	glTexCoord2f(1.0, 1.0); glVertex3f(50.0, 2.6, -30.1);
	glTexCoord2f(1.0, 0.0); glVertex3f(50.0, -2.6, -30.1);
	glEnd();

	//left side
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-50.01, -2.6, -30.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(-50.01, 2.6, -30.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(-50.01, 2.6, 30.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(-50.01, -2.6, 30.0);
	glEnd();

	//right side
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(50.01, -2.6, -30.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(50.01, 2.6, -30.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(50.01, 2.6, 30.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(50.01, -2.6, 30.0);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void BookTexture() {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_FLAT);

	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

	//front 
	glPushMatrix();
	glTranslatef(23.0, 5.0, -15.8); // front left bottom corner
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(0.0, 0.0, 0.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, 20.0, 0.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(12.0, 20.0, 0.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(12.0, 0.0, 0.0);
	glEnd();
	//glDisable(GL_TEXTURE_2D);

	// back
	glPushMatrix();
	glTranslatef(0.0, 0.0, -14.3); // left bottom back corner
	glBindTexture(GL_TEXTURE_2D, texture[4]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(0.0, 0.0, 0.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, 20.0, 0.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(12.0, 20.0, 0.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(12.0, 0.0, 0.0);
	glEnd();
	//glDisable(GL_TEXTURE_2D);

	// left side
	//glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(-0.01, 0.0, 0.0); // move right a bit
	glBindTexture(GL_TEXTURE_2D, texture[5]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(0.0, 0.0, 0.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, 20.0, 0.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(0.0, 20.0, 14.3);
	glTexCoord2f(1.0, 0.0); glVertex3f(0.0, 0.0, 14.3);
	glEnd();
	glPopMatrix(); 
	//glDisable(GL_TEXTURE_2D);
	
	// right cover
	//glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(12.3, 0.0, 14.3); //right bottom front corner
	glBindTexture(GL_TEXTURE_2D, texture[6]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(0.0, 0.0, 0.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, 20.0, 0.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(0.0, 20.0, -14.3);
	glTexCoord2f(1.0, 0.0); glVertex3f(0.0, 0.0, -14.3);
	glEnd();
	glPopMatrix();
	//glDisable(GL_TEXTURE_2D);
	
	glPopMatrix(); // location of back
	glPopMatrix(); // location for front

	// top
	//glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(23.0, 25.3, -15.8); // front left upper corner
	glBindTexture(GL_TEXTURE_2D, texture[4]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(0.0, 0.0, 0.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, 0.0, -14.3);
	glTexCoord2f(1.0, 1.0); glVertex3f(12.0, 0.0, -14.3);
	glTexCoord2f(1.0, 0.0); glVertex3f(12.0, 0.0, 0.0);
	glEnd();
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);

}

//books on table
void books() {
	//book position
	glEnable(GL_COLOR_MATERIAL);
	glPushMatrix();
	glTranslatef(25.0, 15.0, -23.0);

	//first book
	glPushMatrix();
	glScalef(4.0, 20.0, 14.0);
	glColor3f(1.0, 1.0, 1.0);
	glutSolidCube(1.0);
	glColor3f(0.0, 0.0, 0.0);
	glutWireCube(1.0);
	glPopMatrix();

	//second book
	glPushMatrix();
	glTranslatef(4.1, 0.0, 0.0);
	glPushMatrix();
	glScalef(4.0, 20.0, 14.0);
	glColor3f(1.0, 1.0, 1.0);
	glutSolidCube(1.0);
	glColor3f(0.0, 0.0, 0.0);
	glutWireCube(1.0);
	glPopMatrix();

	//third book
	glPushMatrix();
	glTranslatef(4.1, 0.0, 0.0);
	glPushMatrix();
	glScalef(4.0, 20.0, 14.0);
	glColor3f(1.0, 1.0, 1.0);
	glutSolidCube(1.0);
	glColor3f(0.0, 0.0, 0.0);
	glutWireCube(1.0);
	glPopMatrix();

	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
	glDisable(GL_COLOR_MATERIAL);
	
}

void toy() {
	//base
	glEnable(GL_COLOR_MATERIAL);
	glPushMatrix();
	glTranslatef(30.0, 27.5, -20.0);
	glColor3f(250/255.0, 227.0/255.0, 0.0);
	glutSolidSphere(2.5, 50.0, 10.0);
	//line
	glPushMatrix();
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glColor3f(255.0/ 255.0, 192.0 / 255.0, 0.0);
	glutWireSphere(2.6, 5.0, 5.0);
	glPopMatrix();

	//upper
	glColor3f(250 / 255.0, 227.0 / 255.0, 0.0);
	glTranslatef(0.0, 4.0, 0.0);
	glutSolidSphere(1.5, 50.0, 10.0);
	//line
	glPushMatrix();
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glColor3f(255.0 / 255.0, 192.0 / 255.0, 0.0);
	glutWireSphere(1.6, 5.0, 5.0);
	glPopMatrix();

	//hat
	glTranslatef(0.0, 2.5, 0.0);
	glColor3f(189.0/ 255.0, 215.0/ 255.0, 238.0/255.0);
	glutSolidSphere(1.0, 50.0, 10.0);

	glPopMatrix();
	glDisable(GL_COLOR_MATERIAL);
}


void pad() {
	glPushMatrix();
	glTranslatef(0.0, 2.5, 0.0);

	glPushMatrix();
	glScalef(33.0, 0.5, 21.0);
	glColor3f(1.0, 1.0, 1.0);//white
	glutSolidCube(1.0);
	glColor3f(0.0, 0.0, 0.0); //black wire
	glutWireCube(1.0);

	glPopMatrix(); // main part scale
	glPopMatrix(); // pad position
}


void displayObject()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fltFOV, 1, 1, 5000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(X0 * fltZoom, Y0 * fltZoom, Z0 * fltZoom, xref, yref, zref, Vx, Vy, Vz);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2, 0.2, 0.8, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glPushMatrix();
	glRotatef(fltViewingAngle, 0.0, 0.1, 0.0);

	desk();
	books();
	toy();
	pad(); 
	//Load texture for following
	LoadTexture();
	Drawing();
	BookTexture();
	TableTexture();

	glPopMatrix();
	
	glutSwapBuffers();
}

void reshapeWindow(GLint intNewWidth, GLint intNewHeight)
{
	glViewport(0, 0, intNewWidth, intNewHeight);
}


void adjustDisplay(unsigned char key, int x, int y)
{
	if (key == 's' || key == 'S')
		if (Y0 > 25)
			Y0 -= 5; //Camera down
	if (key == 'w' || key == 'W')
		if (Y0 < 500)
			Y0 += 5; //Camera up
	if (key == 'i' || key == 'I')
		if (fltZoom > 0.2)
			fltZoom -= 0.1; //Zoom in
	if (key == 'o' || key == 'O')
		if (fltZoom < 5.0)
			fltZoom += 0.1; //Zoom out
	if (key == 'A' || key == 'a') {
		fltViewingAngle += 10.0; // rotate the scene to left
	}
	if (key == 'D' || key == 'd') {
		fltViewingAngle -= 10.0; // rotate the scene right
	}
	if (key == 'B' || key == 'b') {  // reset to original viewing scene
		X0 = 0.0, Y0 = 50.0, Z0 = 200.0; 
		xref = 0.0, yref = 50.0, zref = 0.0;
		fltZoom = 1.0;
		fltViewingAngle = 0;
	}
	if (key == 'Q' || key == 'q') {
		exit(0);
	}

	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(intWinWidth, intWinHeight);
	glutCreateWindow("Room");
	glutDisplayFunc(displayObject);
	glutReshapeFunc(reshapeWindow);

	lighting();
	
	glutKeyboardFunc(adjustDisplay);
	glutMainLoop();
}